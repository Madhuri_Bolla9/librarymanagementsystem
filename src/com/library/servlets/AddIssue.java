package com.library.servlets;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.library.model.Issue;
import com.library.model.LMSAdmin;
import com.library.model.dao.IssueDAO;
import com.library.utils.DateUtils;

public class AddIssue extends BaseLMSServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doServiceGet(HttpServletRequest request, HttpServletResponse response) {
		setAttribute("issuedOn", DateUtils.formatDate(new Date(), DateUtils.DATE_UI_PICKERFORMAT));
		setIncludeJsp("AddIssue.jsp");
	}

	protected void doServicePost(HttpServletRequest request, HttpServletResponse response) {
		String bookISBN = request.getParameter("bookISBN");
		String userLMSID = request.getParameter("userLMSID");
		String issuedOn = request.getParameter("issuedOn");
		String returnedOn = request.getParameter("returnedOn");
		String adminLMSID = ((LMSAdmin) request.getSession().getAttribute("admin")).getLMSID();
		
		if(bookISBN != null && !"".equals(bookISBN)){
			List<Issue> issues = IssueDAO.getIssues(bookISBN, null, true);
			if(issues.size() > 0){
				setAttribute("error", "Book already issued to " + issues.get(0).getUser().getName());
				setIncludeJsp("AddIssue.jsp");
			}else{
				Date returnedDate = DateUtils.parseDate(returnedOn, DateUtils.DATE_UI_PICKERFORMAT);
				if(IssueDAO.createIssue(bookISBN, userLMSID, adminLMSID, DateUtils.parseDate(issuedOn, DateUtils.DATE_UI_PICKERFORMAT), returnedDate==null?null:adminLMSID, returnedDate)){
					setRedirectLocation("/LMS/Issues?bookISBN=" + bookISBN);
				}else{
					setAttribute("error", "Error in adding a new Issue! Please try again!");
					setIncludeJsp("AddIssue.jsp");
				}
			}
		}else{
			setIncludeJsp("AddIssue.jsp");
		}
		
		setAttribute("bookISBN", bookISBN);
		setAttribute("userLMSID", userLMSID);
		setAttribute("issuedOn", issuedOn);
		setAttribute("returnedOn",returnedOn);
	}
}
