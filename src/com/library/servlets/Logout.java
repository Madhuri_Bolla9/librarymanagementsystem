package com.library.servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Logout extends BaseLMSServlet {
	private static final long serialVersionUID = 1L;

	protected void doServiceGet(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().invalidate();
		setRedirectLocation("/LMS/Login");
	}

	protected void doServicePost(HttpServletRequest request, HttpServletResponse response) {
		doServiceGet(request, response);
	}
}
