package com.library.servlets;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.library.model.dao.BookDAO;
import com.library.utils.DateUtils;

public class AddBook extends BaseLMSServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doServiceGet(HttpServletRequest request, HttpServletResponse response) {
		setIncludeJsp("AddBook.jsp");
	}

	protected void doServicePost(HttpServletRequest request, HttpServletResponse response) {
		String bookISBN = request.getParameter("bookISBN");
		String name = request.getParameter("name");
		String author = request.getParameter("author");
		String publishedby = request.getParameter("publishedBy");
		String publishedon = request.getParameter("publishedOn");
		if(bookISBN != null && !"".equals(bookISBN)){
			if(BookDAO.getBooks(bookISBN, null).size() > 0){
				setAttribute("error", "Book already listed with this isbn");
				setIncludeJsp("AddBook.jsp");
			}else{
				Date addedOn = new Date();
				if(BookDAO.createBook(bookISBN ,name, author, DateUtils.parseDate(publishedon, DateUtils.DATE_UI_PICKERFORMAT), publishedby, addedOn)){
					setRedirectLocation("/LMS/Books?bookISBN=" + bookISBN);
				}else{
					setAttribute("error", "Error in adding a new Book! Please try again!");
					setIncludeJsp("AddBook.jsp");
				}
			}
		}else{
			setIncludeJsp("AddBook.jsp");
		}
		
		setAttribute("bookISBN", bookISBN);
		setAttribute("name", name);
		setAttribute("publishedOn", publishedon);
		setAttribute("publishedBy", publishedby);
		setAttribute("author", author);
	}
}
