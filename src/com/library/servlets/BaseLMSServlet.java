package com.library.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class BaseLMSServlet extends HttpServlet {
	private static final long serialVersionUID = 3634726818119459285L;

	private static Logger logger = Logger.getLogger(BaseLMSServlet.class
			.toString());

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		initRequest();
		if(doAuthCheck(req, resp)){
			doServiceGet(req, resp);
			sendResponse(req, resp);
		}else{
			resp.sendRedirect("/LMS/Login");
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		initRequest();
		if(doAuthCheck(req, resp)){
			doServicePost(req, resp);
			sendResponse(req, resp);
		}else{
			resp.sendRedirect("/LMS/Login");
		}
	}
	
	private void initRequest() {
		attrMap.clear();
		redirectLocation = includeJSP = null;
		
	}
	
	private boolean doAuthCheck(HttpServletRequest req, HttpServletResponse resp) {
		if(req.getServletPath().equals("/Login")){
			return true;
		}
		if(req.getSession().getAttribute("admin") == null){
			return false;
		}
		return true;
	}

	Map<String, Object> attrMap = new HashMap<String, Object>();

	protected void setAttribute(String attribute, Object value) {
		attrMap.put(attribute, value);
	}

	private String redirectLocation;

	protected void setRedirectLocation(String location) {
		redirectLocation = location;
	}

	private String includeJSP;

	protected void setIncludeJsp(String JSP) {
		includeJSP = JSP;
	}

	protected void sendResponse(HttpServletRequest req, HttpServletResponse resp) {
		try {
			for (Map.Entry<String, Object> attrEntry : attrMap.entrySet()) {
				req.setAttribute(attrEntry.getKey(), attrEntry.getValue());
			}
			if (redirectLocation != null && !"".equals(redirectLocation)) {
				resp.sendRedirect(redirectLocation);
			} else {
				if(includeJSP != null){
					req.setAttribute("includePage", includeJSP);
				}
				RequestDispatcher dispatcher = req.getRequestDispatcher("MasterPage.jsp");
				dispatcher.forward(req, resp);
			}
			
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
	}

	protected abstract void doServiceGet(HttpServletRequest req,
			HttpServletResponse resp);

	protected abstract void doServicePost(HttpServletRequest req,
			HttpServletResponse resp);
	
}
