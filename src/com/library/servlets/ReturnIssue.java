package com.library.servlets;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.library.model.Issue;
import com.library.model.LMSAdmin;
import com.library.model.dao.IssueDAO;

public class ReturnIssue extends BaseLMSServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doServiceGet(HttpServletRequest request, HttpServletResponse response) {
		String bookISBN = request.getParameter("bookISBN");
		
		if(bookISBN != null && !"".equals(bookISBN)){
			List<Issue> issues = IssueDAO.getIssues(bookISBN, null, true);
			if(issues.size() == 0){
				setAttribute("error", "No issue is recorded for book with ISBN " + bookISBN + ". Add an issue and then return.");
				setIncludeJsp("ReturnIssue.jsp");
			}else{
				Date returnedOn = new Date(); 
				String adminLMSID = ((LMSAdmin) request.getSession().getAttribute("admin")).getLMSID();
				if(IssueDAO.returnIssue(bookISBN, adminLMSID, returnedOn)){
					setRedirectLocation("/LMS/Issues?bookISBN=" + bookISBN);
				}else{
					setAttribute("error", "Error returning Issue! Please try again!");
					setIncludeJsp("ReturnIssue.jsp");
				}
			}
		}else{
			setIncludeJsp("ReturnIssue.jsp");
		}
		
		setAttribute("bookISBN", bookISBN);
	}

	protected void doServicePost(HttpServletRequest request, HttpServletResponse response) {
		doServiceGet(request, response);
	}
}
