package com.library.servlets.datasources;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.library.model.LMSUser;
import com.library.model.dao.LMSUserDAO;

public class LMSUser_DS extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String searchTerm = request.getParameter("term");
		List<LMSUser> users = LMSUserDAO.getUsers_DS(searchTerm);
		request.setAttribute("users", users);
		RequestDispatcher dispatcher = request.getRequestDispatcher("LMSUser_DS.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
