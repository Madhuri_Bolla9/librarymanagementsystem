package com.library.servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Home extends BaseLMSServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doServiceGet(HttpServletRequest request, HttpServletResponse response) {
		setIncludeJsp("Home.jsp");
	}

	protected void doServicePost(HttpServletRequest request, HttpServletResponse response) {
		doServiceGet(request, response);
	}

}
