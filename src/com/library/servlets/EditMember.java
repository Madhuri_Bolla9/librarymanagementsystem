package com.library.servlets;

import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.library.model.LMSUser;
import com.library.model.dao.LMSUserDAO;

public class EditMember extends BaseLMSServlet implements Servlet {
	private static final long serialVersionUID = 1L;

	protected void doServiceGet(HttpServletRequest request, HttpServletResponse response) {
		String userLMSID = request.getParameter("userLMSID");
		if(userLMSID != null && !"".equals(userLMSID)){
			int lmsID = 0;
			try {
				lmsID = Integer.parseInt(userLMSID);
			}catch(Exception ex) {
				System.err.println("Edit member failed");
			}
			List<LMSUser> users = LMSUserDAO.getUsers(lmsID, null);
			if(users.size() > 0){
				setAttribute("userLMSID", userLMSID);
				LMSUser user = users.get(0);
				setAttribute("name", user.getName());
				setAttribute("phone", user.getPhone());
				setAttribute("email", user.getEmail());
				setAttribute("address", user.getAddress());
			}
		}
		setIncludeJsp("EditMember.jsp");
	}

	protected void doServicePost(HttpServletRequest request, HttpServletResponse response) {
		String userLMSID = request.getParameter("userLMSID");
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		if(userLMSID != null && !"".equals(userLMSID)){
			int lmsID = 0;
			try {
				lmsID = Integer.parseInt(userLMSID);
			}catch(Exception ex) {
				System.err.println("Edit member failed");
			}
			List<LMSUser> users = LMSUserDAO.getUsers(lmsID, null);
			if(users.size() == 0){
				setAttribute("error", "User doesnt exist with this LMS ID");
				setIncludeJsp("EditMember.jsp");
			}else{
				if(LMSUserDAO.updateUser(userLMSID, name, phone, email, address, users.get(0).getDOJ())){
					setRedirectLocation("/LMS/Members?userLMSID=" + userLMSID);
				}else{
					setAttribute("error", "Error updating member! Please try again!");
					setIncludeJsp("EditMember.jsp");
				}
			}
		}else{
			setIncludeJsp("EditMember.jsp");
		}
		setAttribute("userLMSID", userLMSID);
		setAttribute("name", name);
		setAttribute("phone", phone);
		setAttribute("email", email);
		setAttribute("address", address);
	}
}
