package com.library.servlets;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.library.model.Book;
import com.library.model.dao.BookDAO;
import com.library.utils.DateUtils;

public class EditBook extends BaseLMSServlet {
	private static final long serialVersionUID = 1L;

	protected void doServiceGet(HttpServletRequest request, HttpServletResponse response) {
		String bookISBN = request.getParameter("bookISBN");
		if(bookISBN != null && !"".equals(bookISBN)){
			List<Book> books = BookDAO.getBooks(bookISBN, null);
			if(books.size() > 0){
				Book book = books.get(0);
				setAttribute("bookISBN", book.getISBN());
				setAttribute("name", book.getName());
				setAttribute("publishedOn", DateUtils.formatDate(book.getPubDate(), DateUtils.DATE_UI_PICKERFORMAT));
				setAttribute("publishedBy", book.getPublishedBy());
				setAttribute("author", book.getAuthor());
			}
		}
		setIncludeJsp("EditBook.jsp");
	}

	protected void doServicePost(HttpServletRequest request, HttpServletResponse response) {
		String bookISBN = request.getParameter("bookISBN");
		String name = request.getParameter("name");
		String author = request.getParameter("author");
		String publishedby = request.getParameter("publishedBy");
		String publishedon = request.getParameter("publishedOn");
		if(bookISBN != null && !"".equals(bookISBN)){
			List<Book> books = BookDAO.getBooks(bookISBN, null);
			if(books.size() == 0){
				setAttribute("error", "Book doesnt exist with this isbn");
				setIncludeJsp("EditBook.jsp");
			}else{
				Date addedOn = books.get(0).getAddedOn();
				if(BookDAO.updateBook(bookISBN ,name, author, DateUtils.parseDate(publishedon, DateUtils.DATE_UI_PICKERFORMAT), publishedby, addedOn)){
					setRedirectLocation("/LMS/Books?bookISBN=" + bookISBN);
				}else{
					setAttribute("error", "Error in adding a new Book! Please try again!");
					setIncludeJsp("EditBook.jsp");
				}
			}
		}else{
			setIncludeJsp("EditBook.jsp");
		}
		
		setAttribute("bookISBN", bookISBN);
		setAttribute("name", name);
		setAttribute("publishedOn", publishedon);
		setAttribute("publishedBy", publishedby);
		setAttribute("author", author);
	}

}
