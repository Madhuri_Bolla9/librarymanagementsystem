package com.library.servlets;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.library.model.LMSUser;
import com.library.model.dao.LMSUserDAO;

public class Members extends BaseLMSServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doServiceGet(HttpServletRequest request, HttpServletResponse response) {
		String userLMSID = request.getParameter("userLMSID");
		int lmsID = 0;
		try {
			lmsID = Integer.parseInt(userLMSID);
		}catch(Exception ex) {
			System.err.println("Edit member failed");
		}
		List<LMSUser> lmsUsers = LMSUserDAO.getUsers(lmsID, null);
		setAttribute("userLMSID", userLMSID);
		setAttribute("lmsusers", lmsUsers);
		setIncludeJsp("Members.jsp");
	}

	protected void doServicePost(HttpServletRequest request, HttpServletResponse response)  {
		doServiceGet(request, response);
	}

}
