package com.library.servlets;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.library.model.Issue;
import com.library.model.dao.IssueDAO;

public class Issues extends BaseLMSServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doServiceGet(HttpServletRequest request, HttpServletResponse resp) {
		String bookISBN = request.getParameter("bookISBN");
		String userLMSID = request.getParameter("userLMSID");
		String openIssues = request.getParameter("openIssues");
		List<Issue> issues = IssueDAO.getIssues(bookISBN, userLMSID, "on".equals(openIssues)?true:false);
		setAttribute("bookISBN", bookISBN);
		setAttribute("userLMSID", userLMSID);
		setAttribute("issues", issues);
		setAttribute("openIssues", openIssues);
		setIncludeJsp("Issues.jsp");
	}

	@Override
	protected void doServicePost(HttpServletRequest request,
			HttpServletResponse resp) {
		doServiceGet(request, resp);
	}
       
}
