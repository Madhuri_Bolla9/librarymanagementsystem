package com.library.servlets;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.library.model.dao.LMSUserDAO;

public class AddMember extends BaseLMSServlet {
	private static final long serialVersionUID = 1L;

	protected void doServiceGet(HttpServletRequest request, HttpServletResponse response) {
		setIncludeJsp("AddMember.jsp");
	}

	protected void doServicePost(HttpServletRequest request, HttpServletResponse response) {
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		if(email != null && !"".equals(email)){
			if(LMSUserDAO.getUsers(email).size() > 0){
				setAttribute("error", "User already registered with this email");
				setIncludeJsp("AddMember.jsp");
			}else{
				Date doj = new Date();
				int lmsID = LMSUserDAO.createUser( name, phone, email, address, doj);
				if(lmsID > 0){
					setRedirectLocation("/LMS/Members?lmsid=" + lmsID);
				}else{
					setAttribute("error", "Error in adding a new member! Please try again!");
					setIncludeJsp("AddMember.jsp");
				}
			}
		}else{
			setIncludeJsp("AddMember.jsp");
		}
		setAttribute("name", name);
		setAttribute("phone", phone);
		setAttribute("email", email);
		setAttribute("address", address);
	}
}
