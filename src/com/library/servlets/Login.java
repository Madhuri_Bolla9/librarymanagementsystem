package com.library.servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.library.model.LMSAdmin;
import com.library.model.dao.LMSAdminDAO;
import com.library.utils.HashingUtil;

public class Login extends BaseLMSServlet {
	private static final long serialVersionUID = 1L;

	//private static Logger logger = Logger.getLogger(Login.class.toString());

	protected void doServiceGet(HttpServletRequest request,
			HttpServletResponse response) {
		setIncludeJsp("Login.jsp");
	}

	protected void doServicePost(HttpServletRequest request,
			HttpServletResponse response) {
		String lmsid = request.getParameter("lmsid");
		String password = request.getParameter("password");
		LMSAdmin admin = LMSAdminDAO.getUser(lmsid, HashingUtil.hashString(password));
		if(admin != null){
			request.getSession().setAttribute("admin", admin);
			setRedirectLocation("/LMS/Home");
		}else {
			setAttribute("error", "Invalid Login credentials");
			setIncludeJsp("Login.jsp");
		}
	}
}
