package com.library.servlets;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.library.model.Book;
import com.library.model.dao.BookDAO;

public class Books extends BaseLMSServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doServiceGet(HttpServletRequest request, HttpServletResponse resp) {
		String bookISBN = request.getParameter("bookISBN");
		List<Book> books = BookDAO.getBooks(bookISBN, null);
		setAttribute("bookISBN", bookISBN);
		setAttribute("books", books);
		setIncludeJsp("Books.jsp");
	}

	@Override
	protected void doServicePost(HttpServletRequest request,
			HttpServletResponse resp) {
		doServiceGet(request, resp);
	}
       
}
