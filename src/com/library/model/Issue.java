package com.library.model;

import java.util.Calendar;
import java.util.Date;

public class Issue {
	private String issueID;
	private Book book;
	private LMSUser user;
	private LMSAdmin issuedBy;
	private Date issuedOn;
	private LMSAdmin returnedTo;
	private Date returnedOn;
	
	public String getIssueID() {
		return issueID;
	}
	public void setIssueID(String issueID) {
		this.issueID = issueID;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public LMSUser getUser() {
		return user;
	}
	public void setUser(LMSUser user) {
		this.user = user;
	}
	public LMSAdmin getIssuedBy() {
		return issuedBy;
	}
	public void setIssuedBy(LMSAdmin issuedBy) {
		this.issuedBy = issuedBy;
	}
	public Date getIssuedOn() {
		return issuedOn;
	}
	public void setIssuedOn(Date issuedOn) {
		this.issuedOn = issuedOn;
	}
	public LMSAdmin getReturnedTo() {
		return returnedTo;
	}
	public void setReturnedTo(LMSAdmin returnedTo) {
		this.returnedTo = returnedTo;
	}
	public Date getReturnedOn() {
		return returnedOn;
	}
	public void setReturnedOn(Date returnedOn) {
		this.returnedOn = returnedOn;
	}
	
	private boolean OverdueCalculated;
	private int overDueDays;
	public int getOverDueDays(){
		if(issuedOn != null && returnedOn == null && !OverdueCalculated){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(issuedOn);
			calendar.add(Calendar.DAY_OF_MONTH, 15);
			long diffDays = (Calendar.getInstance().getTimeInMillis() - calendar.getTimeInMillis()) / (24 * 60 * 60 * 1000);
			overDueDays = (int) diffDays;
			OverdueCalculated = true;
		}
		return overDueDays;
	}
}
