package com.library.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.library.model.Book;
import com.library.model.Issue;
import com.library.model.LMSAdmin;
import com.library.model.LMSUser;
import com.library.utils.DBUtil;

public class IssueDAO {

	private static Logger logger = Logger
			.getLogger(IssueDAO.class.toString());

	public static List<Issue> getIssues(String bookISBN, String userLMSID, boolean openIssuesOnly) {
		List<Issue> issues = new ArrayList<Issue>();
		try {
			Connection con = DBUtil.getConnection();
			String sql = "SELECT I.ISSUEID, I.BOOKISBN, B.NAME AS BOOKNAME, I.USERLMSID, LU.NAME AS USERNAME, I.ADMINLMSID, LA.NAME AS ADMINNAME, I.ISSUEDON, I.RETURNEDTOADMINLMSID, LA1.NAME AS RETURNEDTOADMINNAME, I.RETURNEDON FROM ISSUES I JOIN BOOKS B ON B.ISBN=I.BOOKISBN JOIN LMSUSERS LU ON LU.LMSID=I.USERLMSID JOIN LMSADMIN LA ON LA.LMSID=I.ADMINLMSID LEFT JOIN LMSADMIN LA1 ON LA1.LMSID=I.RETURNEDTOADMINLMSID";
			String where = "";
			List<String> params = new ArrayList<String>();
			if(!(bookISBN == null || "".equals(bookISBN))){
				where += " bookISBN=?";
				params.add(bookISBN);
			}
			if(!(userLMSID == null || "".equals(userLMSID))){
				where += (where.length() > 0 ? " AND ": "") + " USERLMSID = ?";
				params.add(userLMSID);
			}
			if(openIssuesOnly){
				where += (where.length() > 0 ? " AND ": "") +  " RETURNEDON is null";
			}
			
			PreparedStatement preparedStatement = con
					.prepareStatement(sql + (where.length() == 0?"":" WHERE " + where) + " ORDER BY ISSUEID DESC");
			int i = 0;
			for(String param : params){
				preparedStatement.setString(++i, param);
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				Issue issue = new Issue();
				issue.setIssueID(resultSet.getString("ISSUEID"));
				Book book = new Book();
				book.setISBN(resultSet.getString("BOOKISBN"));
				book.setName(resultSet.getString("BOOKNAME"));
				issue.setBook(book);
				LMSUser user = new LMSUser();
				user.setLMSID(resultSet.getInt("USERLMSID"));
				user.setName(resultSet.getString("USERNAME"));
				issue.setUser(user);
				LMSAdmin issuedByAdmin = new LMSAdmin();
				issuedByAdmin.setLMSID(resultSet.getString("ADMINLMSID"));
				issuedByAdmin.setName(resultSet.getString("ADMINNAME"));
				issue.setIssuedBy(issuedByAdmin);
				issue.setIssuedOn(resultSet.getDate("ISSUEDON"));
				LMSAdmin returnedToAdmin = new LMSAdmin();
				returnedToAdmin.setLMSID(resultSet.getString("RETURNEDTOADMINLMSID"));
				returnedToAdmin.setName(resultSet.getString("RETURNEDTOADMINNAME"));
				issue.setReturnedTo(returnedToAdmin);
				issue.setReturnedOn(resultSet.getDate("RETURNEDON"));
				issues.add(issue);
			}
			resultSet.close();
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return issues;
	}

	public static boolean createIssue(String bookISBN, String userLMSID, String adminLMSID, Date issuedOn, String returnedToAdminLMSID, Date returnedOn) {
		boolean result = false;
		try {
			Connection con = DBUtil.getConnection();
			PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO ISSUES (BOOKISBN, USERLMSID, ADMINLMSID, ISSUEDON, RETURNEDTOADMINLMSID, RETURNEDON) VALUES (?, ?, ?, ?, ?, ?)");
			preparedStatement.setString(1, bookISBN);
			preparedStatement.setString(2, userLMSID);
			preparedStatement.setString(3, adminLMSID);
			preparedStatement.setDate(4, new java.sql.Date(issuedOn.getTime()));
			preparedStatement.setString(5, returnedToAdminLMSID);
			java.sql.Date returnedDate = (returnedOn == null ? null : new java.sql.Date(returnedOn.getTime()));
			preparedStatement.setDate(6, returnedDate);
			preparedStatement.execute();
			result = preparedStatement.getUpdateCount()==1?true:false;
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return result;
	}

	public static boolean returnIssue(String bookISBN, String adminLMSID,
			Date returnedOn) {
		boolean result = false;
		try {
			Connection con = DBUtil.getConnection();
			PreparedStatement preparedStatement = con.prepareStatement("UPDATE ISSUES SET RETURNEDTOADMINLMSID = ?, RETURNEDON = ? WHERE BOOKISBN = ? AND RETURNEDON IS NULL");
			preparedStatement.setString(1, adminLMSID);
			preparedStatement.setDate(2, new java.sql.Date(returnedOn.getTime()));
			preparedStatement.setString(3, bookISBN);
			preparedStatement.execute();
			result = preparedStatement.getUpdateCount()==1?true:false;
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return result;
	}
}
