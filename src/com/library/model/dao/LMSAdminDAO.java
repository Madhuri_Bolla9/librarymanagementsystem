package com.library.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.library.model.LMSAdmin;
import com.library.utils.DBUtil;

public class LMSAdminDAO {

	private static Logger logger = Logger
			.getLogger(LMSAdminDAO.class.toString());

	public static LMSAdmin getUser(String lmsid, String password) {
		LMSAdmin admin = null;
		try {
			Connection con = DBUtil.getConnection();
			PreparedStatement preparedStatement = con
					.prepareStatement("SELECT LMSID, NAME, PHONE, EMAIL, ADDRESS, DOJ FROM LMSADMIN WHERE LMSID=? AND PASSWORD=?");
			preparedStatement.setString(1, lmsid);
			preparedStatement.setString(2, password);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				admin = new LMSAdmin();
				admin.setLMSID(resultSet.getString("LMSID"));
				admin.setName(resultSet.getString("NAME"));
				admin.setPhone(resultSet.getString("PHONE"));
				admin.setEmail(resultSet.getString("EMAIL"));
				admin.setAddress(resultSet.getString("ADDRESS"));
				admin.setDOJ(resultSet.getDate("DOJ"));
			}
			resultSet.close();
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return admin;
	}

}
