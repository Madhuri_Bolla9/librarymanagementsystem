package com.library.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.library.model.LMSUser;
import com.library.utils.DBUtil;

public class LMSUserDAO {

	private static Logger logger = Logger
			.getLogger(LMSUserDAO.class.toString());

	
	public static List<LMSUser> getUsers(int lmsid, String name) {
		List<LMSUser> users = new ArrayList<LMSUser>();
		try {
			Connection con = DBUtil.getConnection();
			String sql = "SELECT LMSID, NAME, PHONE, EMAIL, ADDRESS, DOJ FROM LMSUSERS ";
			String where = "";
			List<String> params = new ArrayList<String>();
			if(!(lmsid == 0)){
				where += " LMSID=?";
				params.add(lmsid + "");
			}
			if(!(name == null || "".equals(name))){
				where += (where.length() > 0 ? " AND ": "") + " NAME LIKE ?";
				params.add(name + "%");
			}
			PreparedStatement preparedStatement = con
					.prepareStatement(sql + (where.length() == 0?"":" WHERE " + where) + " ORDER BY DOJ DESC LIMIT 10");
			int i = 0;
			for(String param : params){
				preparedStatement.setString(++i, param);
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				LMSUser user = new LMSUser();
				user = new LMSUser();
				user.setLMSID(resultSet.getInt("LMSID"));
				user.setName(resultSet.getString("NAME"));
				user.setPhone(resultSet.getString("PHONE"));
				user.setEmail(resultSet.getString("EMAIL"));
				user.setAddress(resultSet.getString("ADDRESS"));
				user.setDOJ(resultSet.getDate("DOJ"));
				users.add(user);
			}
			resultSet.close();
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return users;
	}
	
	public static List<LMSUser> getUsers(String email) {
		List<LMSUser> users = new ArrayList<LMSUser>();
		try {
			Connection con = DBUtil.getConnection();
			String sql = "SELECT LMSID, NAME, PHONE, EMAIL, ADDRESS, DOJ FROM LMSUSERS ";
			String where = "";
			List<String> params = new ArrayList<String>();
			if(!(email == null || "".equals(email))){
				where += " EMAIL=?";
				params.add(email);
			}
			PreparedStatement preparedStatement = con
					.prepareStatement(sql + (where.length() == 0?"":" WHERE " + where) + " ORDER BY DOJ DESC LIMIT 10");
			int i = 0;
			for(String param : params){
				preparedStatement.setString(++i, param);
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				LMSUser user = new LMSUser();
				user = new LMSUser();
				user.setLMSID(resultSet.getInt("LMSID"));
				user.setName(resultSet.getString("NAME"));
				user.setPhone(resultSet.getString("PHONE"));
				user.setEmail(resultSet.getString("EMAIL"));
				user.setAddress(resultSet.getString("ADDRESS"));
				user.setDOJ(resultSet.getDate("DOJ"));
				users.add(user);
			}
			resultSet.close();
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return users;
	}
	
	public static List<LMSUser> getUsers_DS(String searchTerm) {
		List<LMSUser> users = new ArrayList<LMSUser>();
		try {
			Connection con = DBUtil.getConnection();
			String sql = "SELECT LMSID, NAME, PHONE, EMAIL, ADDRESS, DOJ FROM LMSUSERS ";
			String where = "";
			List<String> params = new ArrayList<String>();
			if(!(searchTerm == null || "".equals(searchTerm))){
				where += " LMSID=?";
				params.add(searchTerm);
				where += " OR NAME LIKE ?";
				params.add("%" + searchTerm + "%");
				where += " OR PHONE LIKE ?";
				params.add(searchTerm + "%");
				where += " OR EMAIL LIKE ?";
				params.add(searchTerm + "%");
				where += " OR ADDRESS LIKE ?";
				params.add("%" + searchTerm + "%");
			}
			PreparedStatement preparedStatement = con
					.prepareStatement(sql + (where.length() == 0?"":" WHERE " + where) + " ORDER BY DOJ DESC");
			int i = 0;
			for(String param : params){
				preparedStatement.setString(++i, param);
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				LMSUser user = new LMSUser();
				user = new LMSUser();
				user.setLMSID(resultSet.getInt("LMSID"));
				user.setName(resultSet.getString("NAME"));
				user.setPhone(resultSet.getString("PHONE"));
				user.setEmail(resultSet.getString("EMAIL"));
				user.setAddress(resultSet.getString("ADDRESS"));
				user.setDOJ(resultSet.getDate("DOJ"));
				users.add(user);
			}
			resultSet.close();
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return users;
	}

	public static int createUser(String name, String phone,
			String email, String address, Date doj) {
		boolean result = false;
		try {
			Connection con = DBUtil.getConnection();
			PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO LMSUSERS(NAME, PHONE, EMAIL, ADDRESS, DOJ) VALUES (?, ?, ?, ?, ?)");
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, phone);
			preparedStatement.setString(3, email);
			preparedStatement.setString(4, address);
			preparedStatement.setDate(5, new java.sql.Date(doj.getTime()));
			preparedStatement.execute();
			result = preparedStatement.getUpdateCount()==1?true:false;
			
			if(result) {
				List<LMSUser> users = getUsers(email);
				if(users.size() > 0) {
					int lmsid = users.get(0).getLMSID();
					return lmsid;
				}
			}
			
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return 0;
	}
	
	public static boolean updateUser(String lmsid, String name, String phone,
			String email, String address, Date doj) {
		boolean result = false;
		try {
			Connection con = DBUtil.getConnection();
			PreparedStatement preparedStatement = con.prepareStatement("UPDATE LMSUSERS SET NAME=?, PHONE=?, EMAIL=?, ADDRESS=?, DOJ=? WHERE LMSID=?");
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, phone);
			preparedStatement.setString(3, email);
			preparedStatement.setString(4, address);
			preparedStatement.setDate(5, new java.sql.Date(doj.getTime()));
			preparedStatement.setString(6, lmsid);
			preparedStatement.execute();
			result = preparedStatement.getUpdateCount()==1?true:false;
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return result;
	}
}
