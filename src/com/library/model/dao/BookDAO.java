package com.library.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.library.model.Book;
import com.library.utils.DBUtil;

public class BookDAO {

	private static Logger logger = Logger
			.getLogger(BookDAO.class.toString());

	public static List<Book> getBooks(String isbn, String name) {
		List<Book> books = new ArrayList<Book>();
		try {
			Connection con = DBUtil.getConnection();
			String sql = "SELECT ISBN, NAME, AUTHOR, PUBLISHEDON, PUBLISHEDBY, ADDEDON FROM BOOKS ";
			String where = "";
			List<String> params = new ArrayList<String>();
			if(!(isbn == null || "".equals(isbn))){
				where += " ISBN=?";
				params.add(isbn);
			}
			if(!(name == null || "".equals(name))){
				where += (where.length() > 0 ? " AND ": "") + " NAME LIKE ?";
				params.add(name + "%");
			}
			PreparedStatement preparedStatement = con
					.prepareStatement(sql + (where.length() == 0?"":" WHERE " + where) + " ORDER BY ADDEDON DESC");
			int i = 0;
			for(String param : params){
				preparedStatement.setString(++i, param);
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				Book book = new Book();
				book.setISBN(resultSet.getString("ISBN"));
				book.setName(resultSet.getString("NAME"));
				book.setAuthor(resultSet.getString("AUTHOR"));
				book.setPubDate(resultSet.getDate("PUBLISHEDON"));
				book.setPublishedBy(resultSet.getString("PUBLISHEDBY"));
				book.setAddedOn(resultSet.getDate("ADDEDON"));
				books.add(book);
			}
			resultSet.close();
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return books;
	}
	
	public static List<Book> getBooks_DS(String searchTerm) {
		List<Book> books = new ArrayList<Book>();
		try {
			Connection con = DBUtil.getConnection();
			String sql = "SELECT ISBN, NAME, AUTHOR, PUBLISHEDON, PUBLISHEDBY, ADDEDON FROM BOOKS ";
			String where = "";
			List<String> params = new ArrayList<String>();
			if(!(searchTerm == null || "".equals(searchTerm))){
				where += " ISBN=?";
				params.add(searchTerm);
				where += " OR NAME like ?";
				params.add("%" + searchTerm + "%");
				where += " OR AUTHOR like ?";
				params.add("%" + searchTerm + "%");
				where += " OR PUBLISHEDBY like ?";
				params.add("%" + searchTerm + "%");
			}
			PreparedStatement preparedStatement = con
					.prepareStatement(sql + (where.length() == 0?"":" WHERE " + where) + " ORDER BY ADDEDON DESC");
			int i = 0;
			for(String param : params){
				preparedStatement.setString(++i, param);
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				Book book = new Book();
				book.setISBN(resultSet.getString("ISBN"));
				book.setName(resultSet.getString("NAME"));
				book.setAuthor(resultSet.getString("AUTHOR"));
				book.setPubDate(resultSet.getDate("PUBLISHEDON"));
				book.setPublishedBy(resultSet.getString("PUBLISHEDBY"));
				book.setAddedOn(resultSet.getDate("ADDEDON"));
				books.add(book);
			}
			resultSet.close();
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return books;
	}

	public static boolean createBook(String isbn, String name, String author,
			Date pubDate, String publishedBy, Date addedOn) {
		boolean result = false;
		try {
			Connection con = DBUtil.getConnection();
			PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO BOOKS (ISBN, NAME, AUTHOR, PUBLISHEDON, PUBLISHEDBY, ADDEDON) VALUES (?, ?, ?, ?, ?, ?)");
			preparedStatement.setString(1, isbn);
			preparedStatement.setString(2, name);
			preparedStatement.setString(3, author);
			preparedStatement.setDate(4, new java.sql.Date(pubDate.getTime()));
			preparedStatement.setString(5, publishedBy);
			preparedStatement.setDate(6, new java.sql.Date(addedOn.getTime()));
			preparedStatement.execute();
			result = preparedStatement.getUpdateCount()==1?true:false;
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return result;
	}

	public static boolean updateBook(String isbn, String name,
			String author, Date pubDate, String publishedby, Date addedOn) {
		boolean result = false;
		try {
			Connection con = DBUtil.getConnection();
			PreparedStatement preparedStatement = con.prepareStatement("UPDATE BOOKS SET NAME=?, AUTHOR=?, PUBLISHEDON=?, PUBLISHEDBY=? WHERE ISBN=?");
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, author);
			preparedStatement.setDate(3, new java.sql.Date(pubDate.getTime()));
			preparedStatement.setString(4, publishedby);
			preparedStatement.setString(5, isbn);
			preparedStatement.execute();
			result = preparedStatement.getUpdateCount()==1?true:false;
			preparedStatement.close();
			con.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return result;
	}
}
