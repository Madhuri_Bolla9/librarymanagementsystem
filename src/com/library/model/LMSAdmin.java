/* POJO class for Users in database */

package com.library.model;

import java.util.Date;

public class LMSAdmin {
	private String LMSID;
    private String name;
    private String phone;
    private String email;
    private String address;
    private Date DOJ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDOJ() {
        return DOJ;
    }

    public void setDOJ(Date DOJ) {
        this.DOJ = DOJ;
    }

	public void setLMSID(String lMSID) {
		LMSID = lMSID;
	}

	public String getLMSID() {
		return LMSID;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}
}
