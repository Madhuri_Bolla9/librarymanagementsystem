package com.library.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBUtil {

	private static Logger logger = Logger.getLogger(DBUtil.class.toString());

	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager
					.getConnection("jdbc:mysql://localhost:3306/libmanagement?"
							+ "user=lmsadmin&password=lmsadmin");
			return connection;
		} catch (Exception e) {
			logger.log(Level.SEVERE, "", e);
		}
		return null;
	}
}
