package com.library.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DateUtils {
	
	private static Logger logger = Logger.getLogger(DateUtils.class.toString());
	
	public final static String DATE_DB_FORMAT = "yyyy-MM-dd";
	public final static String DATE_UI_PICKERFORMAT = "MM/dd/yyyy";
	
	
	public static String formatDate(Date date, String format) {
		if(date == null || format == null)
			return "";
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(format);
		return sdf.format(date);
	}
	public static Date parseDate(String date, String format){
		if(date == null || format == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(format);
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			logger.log(Level.SEVERE, "", e); 
		}
		return null;
	}
}
