package com.library.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HashingUtil {

	private static Logger logger = Logger.getLogger(HashingUtil.class
			.toString());

	public static String hashString(String input) {
		String returnHash = null;
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			byte[] digest = messageDigest.digest(input.getBytes());
			BigInteger bigInt = new BigInteger(1, digest);
			StringBuilder hashtext = new StringBuilder(bigInt.toString(16));
			while (hashtext.length() < 32) {
				hashtext.insert(0, "0");
			}
			returnHash = hashtext.toString();
		} catch (NoSuchAlgorithmException e) {
			logger.log(Level.SEVERE, "Error trying to hash input text", e);
		}
		return returnHash;
	}
	
	public static void main(String[] args) {
		System.out.println(hashString("password"));
	}
}
