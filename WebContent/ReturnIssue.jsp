<form method="post" action="/LMS/ReturnIssue" class="returnIssue"
	onsubmit="return validateReturnIssue()">
<fieldset><legend>Return Issue</legend>
<table>
	<tr>
		<td class="label">Book ISBN</td>
		<td><input name="bookISBN" value="${bookISBN}"></td>
	</tr>
	<tr>
		<td colspan="5" align="center"><input type="submit" value="Return">
		<input type="reset" value="Reset" onclick="setErr('&nbsp;')">
		</td>
	</tr>
</table>
</fieldset>
<BR />
<span id="error">${error}&nbsp;</span></form>
<script>
	$(function() {
		initReturnIssue();
	});
</script>