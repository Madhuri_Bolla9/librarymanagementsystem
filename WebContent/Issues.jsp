<%@page import="com.library.utils.DateUtils"%>
<%@page import="com.library.model.Issue"%>
<%@page import="com.library.model.LMSUser"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form>
<table>
	<tr>
		<td>Book</td>
		<td><input name="bookISBN" value="${bookISBN}"></td>
		<td>User</td>
		<td><input name="userLMSID" value="${userLMSID}"></td>
		<td>Open Issues Only</td>
		<td><input name="openIssues" type="checkbox" 
			<c:choose>
				<c:when test="${openIssues == 'on'}">
					checked
				</c:when>
			</c:choose>
		></td>
		<td><input type="submit" value="Search"></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><a href="/LMS/AddIssue">Add Issue</a></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><a href="/LMS/ReturnIssue">Return Issue</a></td>
	</tr>
</table>
</form>
<H2>Issues</H2>
<table class="lmsgrid">
	<tr>
		<th>Book</th>
		<th>Issued To</th>
		<th>Issued By</th>
		<th>Issued On</th>
		<th>Returned To</th>
		<th>Returned On</th>
	</tr>
	<% int rowIndex = 0; %>
	<c:forEach items="${issues}" var="issue">
		<tr class="
			<c:choose>
				<c:when test="${issue.overDueDays > 0}">
					overload
				</c:when>
				<c:otherwise>
					<%= rowIndex++ % 2 == 1 ? "evenRow" : "oddRow"  %>
				</c:otherwise>
			</c:choose>
			">
			<td title="${issue.book.ISBN}">${issue.book.name}</td>
			<td title="${issue.user.LMSID}">${issue.user.name}</td>
			<td title="${issue.issuedBy.LMSID}">${issue.issuedBy.name}</td>
			<td><fmt:formatDate type="date" value="${issue.issuedOn}" /></td>
			<c:choose>
				<c:when test="${issue.returnedOn != null}">
					<td title="${issue.returnedTo.LMSID}">${issue.returnedTo.name}</td>
					<td><fmt:formatDate type="date" value="${issue.returnedOn}" /></td>
				</c:when>
				<c:otherwise>
					<td colspan="2" align="center">
						<a href="/LMS/ReturnIssue?bookISBN=${issue.book.ISBN}">
							Return Now
							<c:choose>
								<c:when test="${issue.overDueDays > 0}">
									[Fine: $ ${(issue.overDueDays * 0.2) > 10 ? 10.0 : (issue.overDueDays * 0.2)}]
								</c:when>
							</c:choose>
						</a>
					</td>
				</c:otherwise>
			</c:choose>
		</tr>
	</c:forEach>
</table>
<script>
	$(function() {
		initIssue();
	});
</script>