<form method="post" action="/LMS/AddMember" class="addMember" onsubmit="return validateAddMember()">
	<fieldset>
		<legend>Add Member</legend>
		<table>
			<tr>
				<td class="label">Name</td>
				<td><input name="name" value="${name}"></td>
			</tr>
			<tr>
				<td class="label">Phone</td>
				<td><input name="phone" value="${phone}"></td>
				<td>&nbsp;&nbsp;&nbsp;</td>
				<td class="label">Email</td>
				<td><input name="email" value="${email}"></td>
			</tr>
			<tr>
				<td class="label">Address</td>
				<td colspan="4"><textarea style="width:100%" name="address">${address}</textarea></td>
			</tr>
			<tr>
				<td colspan="5" align="center">
					<input type="submit" value="Add">
					<input type="reset" value="Reset" onclick="setErr('&nbsp;')">
				</td>
			</tr>
		</table>
	</fieldset>
	<BR />
	<span id="error">${error}&nbsp;</span>
</form>
