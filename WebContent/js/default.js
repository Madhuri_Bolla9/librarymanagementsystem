var ui_PickerDateFormat = "mm/dd/yy";

function validateLogin() {
	setErr("&nbsp;");
	var lmsid_field = EN("lmsid")[0];
	var password_field = EN("password")[0];
	if (lmsid_field.value == "" || password_field.value == "") {
		setErr("All fields are mandatory");
		return false;
	}
	return true;
}

function validateAddMember() {
	setErr("&nbsp;");
	var lmsid_field = EN("userLMSID")[0];
	var name_field = EN("name")[0];
	var phone_field = EN("phone")[0];
	var email_field = EN("email")[0];
	var address_field = EN("address")[0];
	if (lmsid_field.value == "" || name_field.value == ""
			|| phone_field.value == "" || email_field.value == ""
			|| address_field.value == "") {
		setErr("All fields are mandatory");
		return false;
	}
	var phoneReg = /^[0-9]{10}$/;
	if (!phoneReg.test(phone_field.value)) {
		setErr("Enter valid 10 digit phone number");
		return false;
	}
	var emailReg = /^[a-z]+[a-z._0-9]*@[a-z]+[a-z._0-9]*[.][a-z]+$/i;
	if (!emailReg.test(email_field.value)) {
		setErr("Enter valid email");
		return false;
	}
	return true;
}

function validateAddBook() {
	setErr("&nbsp;");
	var isbn_field = EN("bookISBN")[0];
	var name_field = EN("name")[0];
	var author_field = EN("author")[0];
	var publishedOn_field = EN("publishedOn")[0];
	var publishedBy_field = EN("publishedBy")[0];
	if (isbn_field.value == "" || name_field.value == ""
			|| author_field.value == "" || publishedOn_field.value == ""
			|| publishedBy_field.value == "") {
		setErr("All fields are mandatory");
		return false;
	}
	return true;
}

function validateAddIssue() {
	setErr("&nbsp;");
	var bookISBN_field = EN("bookISBN")[0];
	var userLMSID_field = EN("userLMSID")[0];
	var issuedOn_field = EN("issuedOn")[0];
	if (bookISBN_field.value == "" || userLMSID_field.value == ""
			|| issuedOn_field.value == "") {
		setErr("All fields are mandatory");
		return false;
	}
	return true;
}

function validateReturnIssue(){
	setErr("&nbsp;");
	var bookISBN_field = EN("bookISBN")[0];
	if (bookISBN_field.value == "") {
		setErr("ISBN is mandatory");
		return false;
	}
	return true;
}

function E(id) {
	return document.getElementById(id);
}

function EN(name) {
	return document.getElementsByName(name);
}

function setErr(error) {
	E("error").innerHTML = error;
	return true;
}

function initAddIssue() {
	$("input[name=issuedOn]").datepicker({
		maxDate : "+0D",
		dateFormat : ui_PickerDateFormat
	});
	initBookISBNDS($("input[name=bookISBN]"));
	initLMSIDDS($("input[name=userLMSID]"));
}

function initReturnIssue() {
	initBookISBNDS($("input[name=bookISBN]"));
}

function initBooks(){
	initBookISBNDS($("input[name=bookISBN]"));
}

function initMembers(){
	initLMSIDDS($("input[name=userLMSID]"));
}

function initIssue(){
	initBookISBNDS($("input[name=bookISBN]"));
	initLMSIDDS($("input[name=userLMSID]"));
}

function initEditMember(){
	initLMSIDDS($("input[name=userLMSID]"));
	$("input[name=userLMSID]").change(function(){
		window.location = "/LMS/EditMember?userLMSID=" + $(this).val();
	});
}

function initAddBook(){
	$("input[name=publishedOn]").datepicker({ maxDate: "+0D", dateFormat: ui_PickerDateFormat });
}

function initEditBook(){
	$("input[name=publishedOn]").datepicker({ maxDate: "+0D", dateFormat: ui_PickerDateFormat });
	initBookISBNDS($("input[name=bookISBN]"));
	$("input[name=bookISBN]").change(function(){
		window.location = "/LMS/EditBook?bookISBN=" + $(this).val();
	});
}

function initBookISBNDS(control){
	var bookCache = {};
	$(control).autocomplete({
		minLength : 2,
		source : function(request, response) {
			var term = request.term;
			if (term in bookCache) {
				response(bookCache[term]);
				return;
			}
			$.getJSON("/LMS/Books_DS", request, function(data, status, xhr) {
				data.splice(data.length - 1, 1);
				bookCache[term] = data;
				response(data);
			});
		},
		select : function(event, ui) {
			$(control).val(ui.item.ISBN);
			$(control).change();
			return false;
		}
	}).autocomplete("instance")._renderItem = renderBookItem;
}

function initLMSIDDS(control){
	var userCache = {};
	$(control).autocomplete({
		minLength : 2,
		source : function(request, response) {
			var term = request.term;
			if (term in userCache) {
				response(userCache[term]);
				return;
			}
			$.getJSON("/LMS/LMSUser_DS", request, function(data, status, xhr) {
				data.splice(data.length - 1, 1);
				userCache[term] = data;
				response(data);
			});
		},
		select : function(event, ui) {
			$(control).val(ui.item.LMSID);
			$(control).change();
			return false;
		}
	}).autocomplete("instance")._renderItem = renderUserItem;
}

function renderBookItem(ul, book) {
	return $("<li style='border-bottom:1px solid #a6e9c2'>").append(
			"<a>" + book.name + " (" + book.ISBN + ")<BR>" + "Author: "
					+ book.author + "<BR>" + "published On: " + book.pubDate
					+ "<BR>" + "Published By: " + book.publishedBy + "</a>")
			.appendTo(ul);
}

function renderUserItem(ul, user) {
	return $("<li style='border-bottom:1px solid #a6e9c2'>").append(
			"<a>" + user.name + " (" + user.LMSID + ")<BR>" + "Phone: "
					+ user.phone + "<BR>" + "Email: " + user.email + "<BR>"
					+ "DOJ: " + user.DOJ + "<BR>" + "Address: " + user.address
					+ "</a>").appendTo(ul);
}
