<%@page import="com.library.model.LMSUser"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form>
	<table>
		<tr>
			<td><input name="userLMSID" value="${userLMSID}"></td>
			<td><input type="submit" value="Search"></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><a href="/LMS/AddMember">Add Member</a></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><a href="/LMS/EditMember">Edit Member</a></td>
		</tr>
	</table>
</form>
<H2>Members</H2>
<table class="lmsgrid">
	<tr>
		<th>LMS ID</th>
		<th>Name</th>
		<th>Phone</th>
		<th>Email</th>
		<th>Address</th>
		<th>DOJ</th>
		<th/>
	</tr>
	<% int rowIndex = 0; %>
	<c:forEach items="${lmsusers}" var="user">
		<tr class="<%= rowIndex++ % 2 == 1 ? "evenRow" : "oddRow"  %>">
			<td>${user.LMSID}</td>
			<td>${user.name}</td>
			<td>${user.phone}</td>
			<td>${user.email}</td>
			<td>${user.address}</td>
			<td><fmt:formatDate type="date" value="${user.DOJ}" /></td>
			<td><a title="Edit" class="editIcon" href="/LMS/EditMember?userLMSID=${user.LMSID}"> </a></td>
		</tr>
	</c:forEach>
</table>
<script>
	$(function() {
		initMembers();
	});
</script>