<%@page import="com.library.model.Book"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<form>
<table>
	<tr>
		<td><input name="bookISBN" value="${bookISBN}"></td>
		<td><input type="submit" value="Search"></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><a href="/LMS/AddBook">Add Book</a></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><a href="/LMS/EditBook">Edit Book</a></td>
	</tr>
</table>
</form>
<H2>Books</H2>
<table class="lmsgrid">
	<tr>
		<th>ISBN</th>
		<th>Name</th>
		<th>Author</th>
		<th>Published On</th>
		<th>Published By</th>
		<th>Added On</th>
		<th/>
	</tr>
	<%
		int rowIndex = 0;
	%>
	<c:forEach items="${books}" var="book">
		<tr class="<%=rowIndex++ % 2 == 1 ? "evenRow" : "oddRow"%>">
			<td>${book.ISBN}</td>
			<td>${book.name}</td>
			<td>${book.author}</td>
			<td><fmt:formatDate type="date" value="${book.pubDate}" /></td>
			<td>${book.publishedBy}</td>
			<td><fmt:formatDate type="date" value="${book.addedOn}" /></td>
			<td><a title="Edit" class="editIcon" href="/LMS/EditBook?bookISBN=${book.ISBN}"> </a></td>
		</tr>
	</c:forEach>
</table>
<script>
	$(function() {
		initBooks();
	});
</script>