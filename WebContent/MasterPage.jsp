<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Library Management System :: Login page</title>
	<link rel="stylesheet" href="css/default.css" />
	<link rel="stylesheet" href="jquery/jquery-ui.min.css" />
	<script src="js/default.js"></script>
	<script src="jquery/jquery.js"></script>
	<script src="jquery/jquery-ui.min.js"></script>
	<script type="text/javascript">window.history.forward();</script>
</head>
<body>
   <div class="body">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="220" height="116" background="./images/logo.jpg">&nbsp;</td>
        <td width="123" background="./images/1_1.jpg">&nbsp;</td>
        <td width="113" background="./images/1_2.jpg">&nbsp;</td>
        <td width="178" background="./images/1_3.jpg">&nbsp;</td>
        <td width="144" background="./images/1_4.jpg">&nbsp;</td>
        <td width="222" background="./images/1_5.jpg">&nbsp;</td>
      </tr>
    </table>
   	<%
   		if(session.getAttribute("admin") != null){
   	%>
	   	<table width="100%">
	   		<tr>
	   			<td>
	   				Hello <b>${admin.name}!</b>
	   			</td>
	   			<td>
			   		<ul class="nav">
						<li><a href="/LMS/Members">Members</a></li>
						<li><a href="/LMS/Books">Books</a></li>
						<li><a href="/LMS/Issues">Issues</a></li>
						<li><a href="/LMS/Logout">Logout</a></li>
					</ul>
				</td>
			</tr>
		</table>
		<HR />
	<%	} %>
	<div class="body-inner">
		<jsp:include page="${includePage}" />
    </div>
    <div class="foot">
    	<HR />
		Library Management System
    </div>
   </div>
</body>
</html>
