<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
[
<c:forEach items="${users}" var="user">
	{
		"LMSID": "${user.LMSID}",
		"name" : "${user.name}",
		"phone" : "${user.phone}",
		"email" : "${user.email}",
		"address" : "${user.address}",
		"DOJ" : "<fmt:formatDate type="date" value="${user.DOJ}" />"
	},
</c:forEach>
{}]