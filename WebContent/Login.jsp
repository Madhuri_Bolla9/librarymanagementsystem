<form method="post" action="/LMS/Login" class="login" onsubmit="return validateLogin()">
	<fieldset>
		<legend>Login</legend>
		<table>
			<tr>
				<td class="label">LMS ID</td>
				<td><input name="lmsid"></td>
			</tr>
			<tr>
				<td class="label">Password</td>
				<td><input name="password" type="password"></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" value="Login">
				<input type="reset" value="Reset" onclick="setErr('&nbsp;')"></td>
			</tr>
		</table>
	</fieldset>
	<BR />
	<span id="error">${error}&nbsp;</span>
</form>
