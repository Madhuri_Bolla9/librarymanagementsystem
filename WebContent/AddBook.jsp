<form method="post" action="/LMS/AddBook" class="addBook" onsubmit="return validateAddBook()">
	<fieldset>
		<legend>Add Book</legend>
		<table>
			<tr>
				<td class="label">ISBN</td>
				<td><input name="bookISBN" value="${bookISBN}">
				</td>
				<td>&nbsp;&nbsp;&nbsp;</td>
				<td class="label">Name</td>
				<td><input name="name" value="${name}">
				</td>
			</tr>
			<tr>
				<td class="label">Author</td>
				<td><input name="author" value="${author}">
				</td>
				<td>&nbsp;&nbsp;&nbsp;</td>
				<td class="label">Published On</td>
				<td><input name="publishedOn" value="${publishedOn}">
				</td>
			</tr>
			<tr>
				<td class="label">Published By</td>
				<td><input name="publishedBy" value="${publishedBy}">
				</td>
				
			</tr>
			<tr>
				<td colspan="5" align="center"><input type="submit" value="Add">
					<input type="reset" value="Reset" onclick="setErr('&nbsp;')">
				</td>
			</tr>
		</table>
	</fieldset>
	<BR /> <span id="error">${error}&nbsp;</span>
</form>
<script>
	$(function(){
		initAddBook();
	});
</script>