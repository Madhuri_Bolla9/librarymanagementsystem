<form method="post" action="/LMS/AddIssue" class="addIssue"
	onsubmit="return validateAddIssue()">
<fieldset><legend>Add Issue</legend>
<table>
	<tr>
		<td class="label">Book ISBN</td>
		<td><input name="bookISBN" value="${bookISBN}"></td>
	</tr>
	<tr>
		<td class="label">User LMSID</td>
		<td><input name="userLMSID" value="${userLMSID}"></td>
	</tr>
	<tr>
		<td class="label">Issued On</td>
		<td><input name="issuedOn" value="${issuedOn}"></td>
	</tr>
	<tr>
		<td colspan="5" align="center"><input type="submit" value="Add">
		<input type="reset" value="Reset" onclick="setErr('&nbsp;')">
		</td>
	</tr>
</table>
</fieldset>
<BR />
<span id="error">${error}&nbsp;</span></form>
<script>
	$(function() {
		initAddIssue();
	});
</script>