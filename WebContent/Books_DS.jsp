<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
[
<c:forEach items="${books}" var="book">
	{
		"ISBN": "${book.ISBN}",
		"name" : "${book.name}",
		"author" : "${book.author}",
		"pubDate" : "<fmt:formatDate type="date" value="${book.pubDate}" />",
		"publishedBy" : "${book.publishedBy}",
		"addedOn" : "<fmt:formatDate type="date" value="${book.addedOn}" />"
	},
</c:forEach>
{}]